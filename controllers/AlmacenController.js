
const Joi = require('@hapi/joi');
const _ = require('lodash');
const BaseController = require('./BaseController');
const RequestHandler = require('../utils/RequestHandler');
const Logger = require('../utils/logger');


const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class AlmacenController extends BaseController {
	static async getAlmacen(req, res) {
		try {
			const result = await super.getList(req, 'Almacen', {
				include: [{
					model: req.app.get('db')['Ubicacion'],
					as: 'ubicaciones',
					attributes: ['direccionLinea1', 'ciudad'],
					include: [{
						model: req.app.get('db')['Pais'],
						as: 'paises',
						
					}]
				}]
			});
			return requestHandler.sendSuccess(res, 'extrayendo datos de almacen')({ result });
		} catch (error) {
			return requestHandler.sendError(req, res, error);
		}
	}

	static async getAlmacenById(req, res) {
		try {
			const reqParam = req.params.id;
			const schema = Joi.object({
				id: Joi.number().integer().min(1)
			});
			const { error } = schema.validate({ id: reqParam });
			requestHandler.validateJoi(error, 400, 'bad Request', 'id de almacen invalido');

			const result = await super.getById(req, 'Almacen', {
				//attributes: ['nombreProducto'],
				include: [{
					model: req.app.get('db')['Ubicacion'],
					as: 'ubicaciones',
					attributes: ['direccionLinea1', 'ciudad']
				}]
			});
			return requestHandler.sendSuccess(res, 'datos de ubicacion extraido')({ result });
		} catch (error) {
			return requestHandler.sendError(req, res, error);

		}
	}

	static async saveAlmacen(req, res) {
		try {
			const data = req.body;
			const schema = Joi.object({
				nombreAlmacen: Joi.string().required(),
				ubicacionId: Joi.number().required(),
			});
			const { error } = schema.validate(req.body);
			requestHandler.validateJoi(error, 400, 'bad Request', error ? error.details[0].message : '');

			const verificate = await super.getList(req, 'Almacen', {
				where: {
					nombreAlmacen: req.body.nombreAlmacen
				}
			});
			requestHandler.validateExistData(verificate, 400, 'dato existente', 'almacen ya existente');

			const result = super.create(req, 'Almacen', data);
			return requestHandler.sendSuccess(res, 'guardando datos de almacen')({ result });
		} catch (error) {
			return requestHandler.sendError(req, res, error);
		}
	}
}

module.exports = AlmacenController;