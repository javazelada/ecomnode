
const Joi = require('@hapi/joi');
const _ = require('lodash');
const BaseController = require('../controllers/BaseController');
const RequestHandler = require('../utils/RequestHandler');
const Logger = require('../utils/logger');

const logger = new Logger();
const requestHandler = new RequestHandler(logger);


class CategoriaController extends BaseController {
    static async getCategorias(req, res) {
        try {
            const result = await super.getList(req, 'Categoria');
            return requestHandler.sendSuccess(res, 'extrayendo datos de Categoria')({ result });
        } catch (error) {
            return requestHandler.sendError(req, res, error);
        }
    }

    static async getCategoriaById(req, res) {
        try {
            const reqParam = req.params.id;
            const schema = Joi.object({
                id: Joi.number().integer().min(1)
            });
            const { error } = schema.validate({ id: reqParam });
            requestHandler.validateJoi(error, 400, 'bad Request', 'id de categoria invalido');

            const result = await super.getById(req, 'Categoria');
            return requestHandler.sendSuccess(res, 'datos de categoria extraido')({ result });
        } catch (error) {
            return requestHandler.sendError(req, res, error);

        }
    }

    static async saveCategorias(req, res) {
        try {
            const schema = Joi.object({
                nombreCategoria: Joi.string().required(),
                descripcion: Joi.string().required()
            });
            const { error } = schema.validate(req.body);
            requestHandler.validateJoi(error, 400, 'bad Request', error ? error.details[0].message : '');
            const verificate = await super.getList(req, 'Categoria', {
                where: {
                    nombreCategoria: req.body.nombreCategoria
                }
            });
            requestHandler.validateExistData(verificate, 400, 'dato existente', 'categoria ya existente');

            const result = await super.create(req, 'Categoria');
            return requestHandler.sendSuccess(res, 'guardando datos de categoria')({ result });
        } catch (error) {
            return requestHandler.sendError(req, res, error);
        }
    }
}

module.exports = CategoriaController;
