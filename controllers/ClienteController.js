
const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));
const _ = require('lodash');
const BaseController = require('./BaseController');
const RequestHandler = require('../utils/RequestHandler');
const Logger = require('../utils/logger');


const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class ClienteController extends BaseController {
  static async getCliente(req, res) {
    try {
      const result = await super.getList(req, 'Cliente', {
        include: [{
          model: req.app.get('db')['Persona'],
          attributes: ['nombres', 'primerApellido']
        },
        {
          model: req.app.get('db')['EmpleadoCliente'],
          attributes: ['cargo', 'departamento'],
          include: [{
            model: req.app.get('db')['companiaCliente'],
            as: 'compania',
            attributes: ['nombreCompania'],
            include: [{
              model: req.app.get('db')['Ubicacion'],
              include: [{
                model: req.app.get('db')['Pais'],
                as: 'paises'
              }]
            }]
          }]
        }],
      });
      return requestHandler.sendSuccess(res, 'extrayendo datos de cliente')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  static async getClienteById(req, res) {
    try {
      const reqParam = req.params.id;
      const schema = Joi.object({
        id: Joi.number().integer().min(1)
      });
      const { error } = schema.validate({ id: reqParam });
      requestHandler.validateJoi(error, 400, 'bad Request', 'id de cliente invalido');

      const result = await super.getById(req, 'Cliente', {
        //attributes: ['nombreProducto'],
        include: [{
          model: req.app.get('db')['Persona'],
          attributes: ['nombres', 'primerApellido']
        }],
        include: [{
          model: req.app.get('db')['EmpleadoCliente'],
          attributes: ['cargo', 'departamento'],
          include: [{
            model: req.app.get('db')['companiaCliente'],
            as: 'compania',
            attributes: ['nombreCompania'],
            include: [{
              model: req.app.get('db')['Ubicacion'],
              include: [{
                model: req.app.get('db')['Pais'],
                as: 'paises'
              }]
            }]
          }]
        }],
      });
      return requestHandler.sendSuccess(res, 'datos de cliente extraido')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);

    }
  }

  static async saveCliente(req, res) {
    try {
      const data = req.body;
      const schema = Joi.object({
        personaId: Joi.number().required(),
        empleadoClienteId: Joi.number().required(),
      });
      const { error } = schema.validate(req.body);
      requestHandler.validateJoi(error, 400, 'bad Request', error ? error.details[0].message : '');

      const verificate = await super.getList(req, 'Cliente', {
        where: {
          personaId: req.body.personaId,
          empleadoClienteId: req.body.empleadoClienteId
        }
      });
      requestHandler.validateExistData(verificate, 400, 'dato existente', 'cliente ya existente');

      const result = super.create(req, 'Cliente', data);
      return requestHandler.sendSuccess(res, 'guardando datos de cliente')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }
}

module.exports = ClienteController;