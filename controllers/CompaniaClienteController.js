
const Joi = require('@hapi/joi');
const _ = require('lodash');
const BaseController = require('./BaseController');
const RequestHandler = require('../utils/RequestHandler');
const Logger = require('../utils/logger');

const logger = new Logger();
const requestHandler = new RequestHandler(logger);


class CompaniaClienteController extends BaseController {
    static async getCompaniaCliente(req, res) {
        try {
            const result = await super.getList(req, 'companiaCliente', {
                include: [{
                    model: req.app.get('db')['Ubicacion'],
                }]
            });
            return requestHandler.sendSuccess(res, 'extrayendo datos de compania')({ result });
        } catch (error) {
            return requestHandler.sendError(req, res, error);
        }
    }

    static async getCompaniaClienteById(req, res) {
        try {
            const reqParam = req.params.id;
            const schema = Joi.object({
                id: Joi.number().integer().min(1)
            });
            const { error } = schema.validate({ id: reqParam });
            requestHandler.validateJoi(error, 400, 'bad Request', 'id de compania invalido');

            const result = await super.getById(req, 'companiaCliente', {
                include: [{
                    model: req.app.get('db')['Ubicacion'],
                    as: 'paises',
                }]
            });
            return requestHandler.sendSuccess(res, 'datos de compania extraido')({ result });
        } catch (error) {
            return requestHandler.sendError(req, res, error);

        }
    }

    static async saveCompaniaCliente(req, res) {
        try {
            const schema = Joi.object({
                nombreCompania: Joi.string().required(),
                ubicacionId: Joi.number().required()
            });
            const { error } = schema.validate(req.body);
            requestHandler.validateJoi(error, 400, 'bad Request', error ? error.details[0].message : '');
            const verificate = await super.getList(req, 'companiaCliente', {
                where: {
                    nombreCompania: req.body.nombreCompania
                }
            });
            requestHandler.validateExistData(verificate, 400, 'dato existente', 'compania ya existente');

            const result = await super.create(req, 'companiaCliente');
            return requestHandler.sendSuccess(res, 'guardando datos de compania')({ result });
        } catch (error) {
            return requestHandler.sendError(req, res, error);
        }
    }
}

module.exports = CompaniaClienteController;
