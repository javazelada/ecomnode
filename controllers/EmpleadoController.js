
const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));
const _ = require('lodash');
const BaseController = require('./BaseController');
const RequestHandler = require('../utils/RequestHandler');
const Logger = require('../utils/logger');


const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class EmpleadoController extends BaseController {
  static async getEmpleado(req, res) {
    try {
      const result = await super.getList(req, 'Empleado', {
        include: [{
          model: req.app.get('db')['Persona'],
          attributes: ['nombres','primerApellido']
        }]
      });
      return requestHandler.sendSuccess(res, 'extrayendo datos de empleado')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  static async getEmpleadoById(req, res) {
    try {
      const reqParam = req.params.id;
      const schema = Joi.object({
        id: Joi.number().integer().min(1)
      });
      const { error } = schema.validate({ id: reqParam });
      requestHandler.validateJoi(error, 400, 'bad Request', 'id de empleado invalido');

      const result = await super.getById(req, 'Empleado', {
        //attributes: ['nombreProducto'],
        include: [{
          model: req.app.get('db')['Persona'],
          attributes: ['nombres', 'primerApellido']
        }]
      });
      return requestHandler.sendSuccess(res, 'datos de empleado extraido')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);

    }
  }

  static async saveEmpleado(req, res) {
    try {
      const data = req.body;
      const schema = Joi.object({
        fechaInicio: Joi.date().format('YYYY-MM-DD').required(),
        fechaFin: Joi.date().format('YYYY-MM-DD'),
        salario: Joi.number().required(),
        cargo: Joi.number().required(),
        personaId: Joi.number().required(),
        encargadoId: Joi.number()
      });
      const { error } = schema.validate(req.body);
      requestHandler.validateJoi(error, 400, 'bad Request', error ? error.details[0].message : '');

      const verificate = await super.getList(req, 'Empleado', {
        where: {
          personaId: req.body.personaId
        }
      });
      requestHandler.validateExistData(verificate, 400, 'dato existente', 'empleado ya existente');

      const result = super.create(req, 'Empleado', data);
      return requestHandler.sendSuccess(res, 'guardando datos de empleado')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }
}

module.exports = EmpleadoController;