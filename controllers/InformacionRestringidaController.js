
const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));
const _ = require('lodash');
const BaseController = require('./BaseController');
const RequestHandler = require('../utils/RequestHandler');
const Logger = require('../utils/logger');


const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class InformacionRestringidaController extends BaseController {
  static async getInformacionRestringida(req, res) {
    try {
      const result = await super.getList(req, 'InformacionRestringida', {
        include: [{
          model: req.app.get('db')['Persona'],
          attributes: ['nombres','primerApellido']
        }]
      });
      return requestHandler.sendSuccess(res, 'extrayendo datos de empleado')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  static async getInformacionRestringidaById(req, res) {
    try {
      const reqParam = req.params.id;
      const schema = Joi.object({
        id: Joi.number().integer().min(1)
      });
      const { error } = schema.validate({ id: reqParam });
      requestHandler.validateJoi(error, 400, 'bad Request', 'id de empleado invalido');

      const result = await super.getById(req, 'InformacionRestringida', {
        //attributes: ['nombreProducto'],
        include: [{
          model: req.app.get('db')['Persona'],
          attributes: ['nombres', 'primerApellido']
        }]
      });
      return requestHandler.sendSuccess(res, 'datos de empleado extraido')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);

    }
  }

  static async saveInformacionRestringida(req, res) {
    try {
      const data = req.body;
      const schema = Joi.object({
        fechaNacimiento: Joi.date().format('YYYY-MM-DD').required(),
        cedulaIdentidad: Joi.string().required(),
        pasaporte: Joi.string(),
        fechaContrato: Joi.date().format('YYYY-MM-DD').required(),
        personaId: Joi.number().required(),
      });
      const { error } = schema.validate(req.body);
      requestHandler.validateJoi(error, 400, 'bad Request', error ? error.details[0].message : '');

      const verificate = await super.getList(req, 'InformacionRestringida', {
        where: {
          cedulaIdentidad: req.body.cedulaIdentidad
        }
      });
      requestHandler.validateExistData(verificate, 400, 'dato existente', 'empleado ya existente');

      const result = super.create(req, 'InformacionRestringida', data);
      return requestHandler.sendSuccess(res, 'guardando datos de empleado')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }
}

module.exports = InformacionRestringidaController;