
const Joi = require('@hapi/joi');
const _ = require('lodash');
const BaseController = require('./BaseController');
const RequestHandler = require('../utils/RequestHandler');
const Logger = require('../utils/logger');

const logger = new Logger();
const requestHandler = new RequestHandler(logger);


class PaisController extends BaseController {
  static async getPais(req, res) {
    try {
      const result = await super.getList(req, 'Pais');
      return requestHandler.sendSuccess(res, 'extrayendo datos de Pais')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  static async getPaisById(req, res) {
    try {
      const reqParam = req.params.id;
      const schema = Joi.object({
        id: Joi.number().integer().min(1)
      });
      const { error } = schema.validate({ id: reqParam });
      requestHandler.validateJoi(error, 400, 'bad Request', 'id de pais invalido');

      const result = await super.getById(req, 'Pais');
      return requestHandler.sendSuccess(res, 'datos de pais extraido')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);

    }
  }

  static async savePais(req, res) {
    try {
      const schema = Joi.object({
        nombrePais: Joi.string().required(),
        codigoLenguaje: Joi.string().required(),
        idiomaPreferido: Joi.number(),
      });
      const { error } = schema.validate(req.body);
      requestHandler.validateJoi(error, 400, 'bad Request', error ? error.details[0].message : '');
      const verificate = await super.getList(req, 'Pais', {
        where: {
          nombrePais: req.body.nombrePais
        }
      });
      requestHandler.validateExistData(verificate, 400, 'dato existente', 'pais ya existente');

      const result = await super.create(req, 'Pais');
      return requestHandler.sendSuccess(res, 'guardando datos de pais')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }
}

module.exports = PaisController;
