
const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));
const _ = require('lodash');
const BaseController = require('./BaseController');
const RequestHandler = require('../utils/RequestHandler');
const Logger = require('../utils/logger');


const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class PedidoController extends BaseController {
  static async getPedido(req, res) {
    try {
      const result = await super.getList(req, 'Pedido', {
        include: [{
          model: req.app.get('db')['Cliente'],
          include: [{
            model: req.app.get('db')['Persona'],
            attributes: ['nombres', 'primerApellido']
          },
          {
            model: req.app.get('db')['EmpleadoCliente'],
            attributes: ['cargo', 'departamento'],
            include: [{
              model: req.app.get('db')['companiaCliente'],
              as: 'compania',
              attributes: ['nombreCompania'],
              include: [{
                model: req.app.get('db')['Ubicacion'],
                include: [{
                  model: req.app.get('db')['Pais'],
                  as: 'paises'
                }]
              }]
            }]
          }],
        }],
      });
      return requestHandler.sendSuccess(res, 'extrayendo datos de pedido')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  static async getPedidoById(req, res) {
    try {
      const reqParam = req.params.id;
      const schema = Joi.object({
        id: Joi.number().integer().min(1)
      });
      const { error } = schema.validate({ id: reqParam });
      requestHandler.validateJoi(error, 400, 'bad Request', 'id de pedido invalido');

      const result = await super.getById(req, 'Pedido', {
        //attributes: ['nombreProducto'],
        include: [{
          model: req.app.get('db')['Cliente'],
          include: [{
            model: req.app.get('db')['Persona'],
            attributes: ['nombres', 'primerApellido']
          },
          {
            model: req.app.get('db')['EmpleadoCliente'],
            attributes: ['cargo', 'departamento'],
            include: [{
              model: req.app.get('db')['companiaCliente'],
              as: 'compania',
              attributes: ['nombreCompania'],
              include: [{
                model: req.app.get('db')['Ubicacion'],
                include: [{
                  model: req.app.get('db')['Pais'],
                  as: 'paises'
                }]
              }]
            }]
          }],
        }],
      });
      return requestHandler.sendSuccess(res, 'datos de pedido extraido')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);

    }
  }

  static async savePedido(req, res) {
    try {
      const data = req.body;
      const schema = Joi.object({
        fechaPedido: Joi.date().format('YYYY-MM-DD').required(),
        codigoPedido: Joi.number().required(),
        totalPedido: Joi.number().required(),
        nonedaPedido: Joi.number(),
        codigoPromocion: Joi.string(),
        clienteId: Joi.number().required()
      });
      const { error } = schema.validate(req.body);
      requestHandler.validateJoi(error, 400, 'bad Request', error ? error.details[0].message : '');

      const verificate = await super.getList(req, 'Pedido', {
        where: {
          codigoPedido: req.body.personaId,
          }
      });
      requestHandler.validateExistData(verificate, 400, 'dato existente', 'pedido ya existente');

      const result = super.create(req, 'Pedido', data);
      return requestHandler.sendSuccess(res, 'guardando datos de pedido')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }
}

module.exports = PedidoController;