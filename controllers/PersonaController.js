
const Joi = require('@hapi/joi');
const _ = require('lodash');
const BaseController = require('./BaseController');
const RequestHandler = require('../utils/RequestHandler');
const Logger = require('../utils/logger');


const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class PersonaController extends BaseController {
  static async getPersona(req, res) {
    try {
      const result = await super.getList(req, 'Persona', {
        include: [{
          model: req.app.get('db')['Usuario'],
          attributes: ['nombreUsuario']
        }]
      });
      return requestHandler.sendSuccess(res, 'extrayendo datos de persona')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  static async getPersonaById(req, res) {
    try {
      const reqParam = req.params.id;
      const schema = Joi.object({
        id: Joi.number().integer().min(1)
      });
      const { error } = schema.validate({ id: reqParam });
      requestHandler.validateJoi(error, 400, 'bad Request', 'id de persona invalido');

      const result = await super.getById(req, 'Persona', {
        //attributes: ['nombreProducto'],
        include: [{
          model: req.app.get('db')['Usuario'],
          attributes: ['nombreUsuario']
        }]
      });
      return requestHandler.sendSuccess(res, 'datos de persona extraido')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);

    }
  }

  static async savePersona(req, res) {
    try {
      const data = req.body;
      const schema = Joi.object({
        nombres: Joi.string().required(),
        primerApellido: Joi.string().required(),
        segundoApellido: Joi.string(),
        nickname: Joi.string().required(),
        idiomaPreferido: Joi.number(),
        genero: Joi.number(),
        usuarioId: Joi.number().required(),
      });
      const { error } = schema.validate(req.body);
      requestHandler.validateJoi(error, 400, 'bad Request', error ? error.details[0].message : '');

      const verificate = await super.getList(req, 'Persona', {
        where: {
          nombres: req.body.nombres
        }
      });
      requestHandler.validateExistData(verificate, 400, 'dato existente', 'persona ya existente');

      const result = super.create(req, 'Persona', data);
      return requestHandler.sendSuccess(res, 'guardando datos de persona')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }
}

module.exports = PersonaController;