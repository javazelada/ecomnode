
const Joi = require('@hapi/joi');
const _ = require('lodash');
const BaseController = require('../controllers/BaseController');
const RequestHandler = require('../utils/RequestHandler');
const Logger = require('../utils/logger');


const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class ProductoController extends BaseController {
    static async getProductos(req, res) {
        try {
            const result = await super.getList(req, 'Producto', {
                include: ['categorias']
            });
            return requestHandler.sendSuccess(res, 'extrayendo datos de Producto')({ result });
        } catch (error) {
            return requestHandler.sendError(req, res, error);
        }
    }

    static async getProductoById(req, res) {
        try {
            const reqParam = req.params.id;
            const schema = Joi.object({
                id: Joi.number().integer().min(1)
            });
            const { error } = schema.validate({ id: reqParam });
            requestHandler.validateJoi(error, 400, 'bad Request', 'id de producto invalido');

            const result = await super.getById(req, 'Producto', {
                //attributes: ['nombreProducto'],
                include: [{
                    model: req.app.get('db')['Categoria'],
                    as: 'categorias',
                    attributes: ['nombreCategoria', 'descripcion']
                }]
            });
            return requestHandler.sendSuccess(res, 'datos de producto extraido')({ result });
        } catch (error) {
            return requestHandler.sendError(req, res, error);

        }
    }

    static async saveProductos(req, res) {
        try {
            const data = req.body;
            const schema = Joi.object({
                nombreProducto: Joi.string().required(),
                descripcion: Joi.string(),
                periodoGarantia: Joi.number(),
                estado: Joi.number(),
                precioMinimo: Joi.number(),
                precioActual: Joi.number(),
                categoriaId: Joi.number().required()
            });
            const { error } = schema.validate(req.body);
            requestHandler.validateJoi(error, 400, 'bad Request', error ? error.details[0].message : '');

            const verificate = await super.getList(req, 'Producto', {
                where: {
                    nombreProducto: req.body.nombreProducto
                }
            });
            requestHandler.validateExistData(verificate, 400, 'dato existente', 'producto ya existente');

            const result = super.create(req, 'Producto', data);
            return requestHandler.sendSuccess(res, 'guardando datos de producto')({ result });
        } catch (error) {
            return requestHandler.sendError(req, res, error);
        }
    }
}

module.exports = ProductoController;