
const Joi = require('@hapi/joi');
const _ = require('lodash');
const BaseController = require('./BaseController');
const RequestHandler = require('../utils/RequestHandler');
const Logger = require('../utils/logger');

const logger = new Logger();
const requestHandler = new RequestHandler(logger);


class RolController extends BaseController {
    static async getRol(req, res) {
        try {
            const result = await super.getList(req, 'Rol');
            return requestHandler.sendSuccess(res, 'extrayendo datos de Rol')({ result });
        } catch (error) {
            return requestHandler.sendError(req, res, error);
        }
    }

    static async getRolById(req, res) {
        try {
            const reqParam = req.params.id;
            const schema = Joi.object({
                id: Joi.number().integer().min(1)
            });
            const { error } = schema.validate({ id: reqParam });
            requestHandler.validateJoi(error, 400, 'bad Request', 'id de rol invalido');

            const result = await super.getById(req, 'Rol');
            return requestHandler.sendSuccess(res, 'datos de rol extraido')({ result });
        } catch (error) {
            return requestHandler.sendError(req, res, error);

        }
    }

    static async saveRol(req, res) {
        try {
            const schema = Joi.object({
                nombreRol: Joi.string().required(),
                descripcion: Joi.string().required()
            });
            const { error } = schema.validate(req.body);
            requestHandler.validateJoi(error, 400, 'bad Request', error ? error.details[0].message : '');
            const verificate = await super.getList(req, 'Rol', {
                where: {
                    nombreRol: req.body.nombreRol
                }
            });
            requestHandler.validateExistData(verificate, 400, 'dato existente', 'rol ya existente');

            const result = await super.create(req, 'Rol');
            return requestHandler.sendSuccess(res, 'guardando datos de rol')({ result });
        } catch (error) {
            return requestHandler.sendError(req, res, error);
        }
    }
}

module.exports = RolController;
