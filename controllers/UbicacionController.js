
const Joi = require('@hapi/joi');
const _ = require('lodash');
const BaseController = require('./BaseController');
const RequestHandler = require('../utils/RequestHandler');
const Logger = require('../utils/logger');


const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class UbicacionController extends BaseController {
	static async getUbicacion(req, res) {
		try {
			const result = await super.getList(req, 'Ubicacion', {
				include: [{
					model: req.app.get('db')['Pais'],
					as: 'paises',
					attributes: ['nombrePais']
				}]
			});
			return requestHandler.sendSuccess(res, 'extrayendo datos de ubicacion')({ result });
		} catch (error) {
			return requestHandler.sendError(req, res, error);
		}
	}

	static async getUbicacionById(req, res) {
		try {
			const reqParam = req.params.id;
			const schema = Joi.object({
				id: Joi.number().integer().min(1)
			});
			const { error } = schema.validate({ id: reqParam });
			requestHandler.validateJoi(error, 400, 'bad Request', 'id de ubicacion invalido');

			const result = await super.getById(req, 'Ubicacion', {
				//attributes: ['nombreProducto'],
				include: [{
					model: req.app.get('db')['Pais'],
					as: 'paises',
					attributes: ['nombrePais']
				}]
			});
			return requestHandler.sendSuccess(res, 'datos de ubicacion extraido')({ result });
		} catch (error) {
			return requestHandler.sendError(req, res, error);

		}
	}

	static async saveUbicacion(req, res) {
		try {
			const data = req.body;
			const schema = Joi.object({
				direccionLinea1: Joi.string().required(),
				direccionLinea2: Joi.string(),
				ciudad: Joi.string(),
				descripcion: Joi.string(),
				paisId: Joi.number().required(),
			});
			const { error } = schema.validate(req.body);
			requestHandler.validateJoi(error, 400, 'bad Request', error ? error.details[0].message : '');

			const verificate = await super.getList(req, 'Ubicacion', {
				where: {
					direccionLinea1: req.body.direccionLinea1
				}
			});
			requestHandler.validateExistData(verificate, 400, 'dato existente', 'ubicacion ya existente');

			const result = super.create(req, 'Ubicacion', data);
			return requestHandler.sendSuccess(res, 'guardando datos de ubicacion')({ result });
		} catch (error) {
			return requestHandler.sendError(req, res, error);
		}
	}
}

module.exports = UbicacionController;