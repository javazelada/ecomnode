
const Joi = require('@hapi/joi');
const _ = require('lodash');
const BaseController = require('./BaseController');
const RequestHandler = require('../utils/RequestHandler');
const Logger = require('../utils/logger');


const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class UsuarioController extends BaseController {
  static async getUsuario(req, res) {
    try {
      const result = await super.getList(req, 'Usuario', {
        include: [{
          model: req.app.get('db')['Rol'],
          as: 'rol',
          attributes: ['nombreRol', 'descripcion']
        }]
      });
      return requestHandler.sendSuccess(res, 'extrayendo datos de Rol')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }

  static async getUsuarioById(req, res) {
    try {
      const reqParam = req.params.id;
      const schema = Joi.object({
        id: Joi.number().integer().min(1)
      });
      const { error } = schema.validate({ id: reqParam });
      requestHandler.validateJoi(error, 400, 'bad Request', 'id de usuario invalido');

      const result = await super.getById(req, 'Usuario', {
        //attributes: ['nombreProducto'],
        include: [{
          model: req.app.get('db')['Rol'],
          as: 'rol',
          attributes: ['nombreRol', 'descripcion']
        }]
      });
      return requestHandler.sendSuccess(res, 'datos de usuario extraido')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);

    }
  }

  static async saveUsuarios(req, res) {
    try {
      const data = req.body;
      const schema = Joi.object({
        nombreUsuario: Joi.string().required(),
        contrasena: Joi.string().required(),
        rolId: Joi.number().required()
      });
      const { error } = schema.validate(req.body);
      requestHandler.validateJoi(error, 400, 'bad Request', error ? error.details[0].message : '');

      const verificate = await super.getList(req, 'Usuario', {
        where: {
          nombreUsuario: req.body.nombreUsuario
        }
      });
      requestHandler.validateExistData(verificate, 400, 'dato existente', 'usuario ya existente');

      const result = super.create(req, 'Usuario', data);
      return requestHandler.sendSuccess(res, 'guardando datos de usuario')({ result });
    } catch (error) {
      return requestHandler.sendError(req, res, error);
    }
  }
}

module.exports = UsuarioController;