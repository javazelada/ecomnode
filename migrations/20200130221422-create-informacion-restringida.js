
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('InformacionRestringidas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fechaNacimiento: {
        type: Sequelize.DATE
      },
      cedulaIdentidad: {
        type: Sequelize.STRING
      },
      pasaporte: {
        type: Sequelize.STRING
      },
      fechaContrato: {
        type: Sequelize.DATE
      },
      personaId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Personas',
          key: 'id',
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('InformacionRestringidas');
  }
};