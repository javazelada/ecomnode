
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Pais', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nombrePais: {
        type: Sequelize.STRING
      },
      codigoLenguaje: {
        type: Sequelize.STRING
      },
      idiomaPreferido: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Pais');
  }
};