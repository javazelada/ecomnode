
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Ubicacions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      direccionLinea1: {
        type: Sequelize.STRING
      },
      direccionLinea2: {
        type: Sequelize.STRING
      },
      ciudad: {
        type: Sequelize.STRING
      },
      descripcion: {
        type: Sequelize.STRING
      },
      paisId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Pais',
          key: 'id',
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Ubicacions');
  }
};