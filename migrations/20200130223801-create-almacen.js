
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Almacens', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nombreAlmacen: {
        type: Sequelize.STRING
      },
      ubicacionId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Ubicacions',
          key: 'id',
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Almacens');
  }
};