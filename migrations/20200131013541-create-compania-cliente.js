
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('companiaClientes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nombreCompania: {
        type: Sequelize.STRING
      },
      ubicacionId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Ubicacions',
          key: 'id',
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('companiaClientes');
  }
};