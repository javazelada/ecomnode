
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('EmpleadoClientes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      cargo: {
        type: Sequelize.STRING
      },
      departamento: {
        type: Sequelize.STRING
      },
      companiaClienteId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'companiaClientes',
          key: 'id',
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('EmpleadoClientes');
  }
};