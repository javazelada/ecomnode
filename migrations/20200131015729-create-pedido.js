
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Pedidos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fechaPedido: {
        type: Sequelize.DATE,
        allowNull: false
      },
      codigoPedido: {
        type: Sequelize.INTEGER
      },
      estadoPedido: {
        type: Sequelize.INTEGER
      },
      totalPedido: {
        type: Sequelize.INTEGER
      },
      monedaPedido: {
        type: Sequelize.INTEGER
      },
      codigoPromocion: {
        type: Sequelize.STRING
      },
      clienteId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Clientes',
          key: 'id',
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Pedidos');
  }
};