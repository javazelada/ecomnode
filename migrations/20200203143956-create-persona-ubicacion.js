
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('personaUbicacions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      personaId: {
        type: Sequelize.INTEGER
      },
      ubicacionId: {
        type: Sequelize.INTEGER
      },
      numeroTel: {
        type: Sequelize.INTEGER
      },
      numeroCel: {
        type: Sequelize.INTEGER
      },
      codigoPais: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('personaUbicacions');
  }
};