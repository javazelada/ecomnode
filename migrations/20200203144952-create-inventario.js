
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Inventarios', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      productoId: {
        type: Sequelize.INTEGER
      },
      almacenId: {
        type: Sequelize.INTEGER
      },
      cantidadObtenida: {
        type: Sequelize.INTEGER
      },
      fechaObtenida: {
        type: Sequelize.DATE
      },
      costoUnitario: {
        type: Sequelize.FLOAT
      },
      cantidadDisponible: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Inventarios');
  }
};