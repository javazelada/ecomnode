
module.exports = (sequelize, DataTypes) => {
  const Almacen = sequelize.define('Almacen', {
    nombreAlmacen: {
      type: DataTypes.STRING,
      allowNull: false
    },
    ubicacionId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
  }, {});
  Almacen.associate = function(models) {
    Almacen.belongsTo(models.Ubicacion, {
      foreignKey: 'ubicacionId',
      as: 'ubicaciones'
    });
  };
  return Almacen;
};