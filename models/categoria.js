
module.exports = (sequelize, DataTypes) => {
    const Categoria = sequelize.define('Categoria', {
        nombreCategoria: {
            type: DataTypes.STRING,
            allowNull: false
        },
        descripcion: {
            type: DataTypes.STRING,
        }
    }, {});
    Categoria.associate = function (models) {
        Categoria.hasMany(models.Producto, {
            foreignKey: 'categoriaId'
        });
    };
    return Categoria;
};