
module.exports = (sequelize, DataTypes) => {
  const Cliente = sequelize.define('Cliente', {
    personaId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    empleadoClienteId: {
      type: DataTypes.INTEGER
    },
  }, {});
  Cliente.associate = function (models) {
    Cliente.belongsTo(models.Persona, {
      foreignKey: 'personaId'
    });
    Cliente.belongsTo(models.EmpleadoCliente, {
      foreignKey: 'empleadoClienteId'
    });
    Cliente.hasMany(models.Pedido, {
      foreignKey: 'clienteId'
    });
  };
  return Cliente;
};