
module.exports = (sequelize, DataTypes) => {
  const companiaCliente = sequelize.define('companiaCliente', {
    nombreCompania: {
      type: DataTypes.STRING,
    },
    ubicacionId: {
      type: DataTypes.INTEGER
    },
  }, {});
  companiaCliente.associate = function(models) {
    companiaCliente.hasMany(models.EmpleadoCliente, {
      foreignKey: 'companiaClienteId'
    });
    companiaCliente.belongsTo(models.Ubicacion, {
      foreignKey: 'ubicacionId',
    });
  };
  return companiaCliente;
};