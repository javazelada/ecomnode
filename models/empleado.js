
module.exports = (sequelize, DataTypes) => {
  const Empleado = sequelize.define('Empleado', {
    fechaInicio: {
      type: DataTypes.DATE,
      allowNull: false
    },
    fechaFin: {
      type: DataTypes.DATE,
    },
    salario: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    cargo: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    personaId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    encargadoId: {
      type: DataTypes.STRING
    },
  }, {});
  Empleado.associate = function (models) {
    Empleado.belongsTo(models.Persona, {
      foreignKey: 'personaId'
    });
  };
  return Empleado;
};