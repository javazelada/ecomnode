
module.exports = (sequelize, DataTypes) => {
  const EmpleadoCliente = sequelize.define('EmpleadoCliente', {
    cargo: {
      type: DataTypes.STRING,
    },
    departamento: {
      type: DataTypes.STRING,
    },
    companiaClienteId: {
      type: DataTypes.INTEGER
    },
  }, {});
  EmpleadoCliente.associate = function (models) {
    EmpleadoCliente.belongsTo(models.companiaCliente, {
      foreignKey: 'companiaClienteId',
      as: 'compania'
    });
  };
  return EmpleadoCliente;
};