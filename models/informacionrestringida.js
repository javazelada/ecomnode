
module.exports = (sequelize, DataTypes) => {
  const InformacionRestringida = sequelize.define('InformacionRestringida', {
    fechaNacimiento: {
      type: DataTypes.DATE,
    },
    cedulaIdentidad: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    pasaporte: {
      type: DataTypes.STRING,
    },
    fechaContrato: {
      type: DataTypes.DATE,
    },
    personaId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
  }, {});
  InformacionRestringida.associate = function (models) {
    InformacionRestringida.belongsTo(models.Persona, {
      foreignKey: 'personaId'
    });
  };
  return InformacionRestringida;
};