
module.exports = (sequelize, DataTypes) => {
  const Inventario = sequelize.define('Inventario', {
    productoId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    almacenId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    cantidadObtenida: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    fechaObtenida: {
      type: DataTypes.DATE,
      allowNull: false
    },
    costoUnitario: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    cantidadDisponible: {
      type: DataTypes.INTEGER,
    },
  }, {});
  Inventario.associate = function(models) {
    // associations can be defined here
  };
  return Inventario;
};