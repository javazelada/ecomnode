
module.exports = (sequelize, DataTypes) => {
  const Pais = sequelize.define('Pais', {
    nombrePais: {
      type: DataTypes.STRING,
      allowNull: false
    },
    codigoLenguaje: {
      type: DataTypes.STRING,
    },
    idiomaPreferido: {
      type: DataTypes.INTEGER
    },
  }, {});
  Pais.associate = function (models) {
    Pais.hasMany(models.Ubicacion, {
      foreignKey: 'paisId'
    });
  };
  return Pais;
};