
module.exports = (sequelize, DataTypes) => {
  const Pedido = sequelize.define('Pedido', {
    fechaPedido: {
      type: DataTypes.DATE,
    },
    codigoPedido: {
      type: DataTypes.INTEGER,
    },
    estadoPedido: {
      type: DataTypes.INTEGER,
    },
    totalPedido: {
      type: DataTypes.INTEGER,
    },
    monedaPedido: {
      type: DataTypes.INTEGER,
    },
    codigoPromocion: {
      type: DataTypes.STRING,
    },
    clienteId: {
      type: DataTypes.INTEGER,
    },
  }, {});
  Pedido.associate = function (models) {
    Pedido.belongsTo(models.Cliente, {
      foreignKey: 'clienteId'
    });
  };
  return Pedido;
};