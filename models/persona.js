module.exports = (sequelize, DataTypes) => {
  const Persona = sequelize.define('Persona', {
    nombres: {
      type: DataTypes.STRING,
      allowNull: false
    },
    primerApellido: {
      type: DataTypes.STRING,
      allowNull: false
    },
    segundoApellido: {
      type: DataTypes.STRING,
    },
    nickname: {
      type: DataTypes.STRING,
      allowNull: false
    },
    idiomaPreferido: {
      type: DataTypes.INTEGER,
    },
    genero: {
      type: DataTypes.INTEGER,
    },
    usuarioId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
  }, {});
  Persona.associate = function (models) {
    Persona.belongsTo(models.Usuario, {
      foreignKey: 'usuarioId'
    });
  };
  return Persona;
};