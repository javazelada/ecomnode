
module.exports = (sequelize, DataTypes) => {
  const personaUbicacion = sequelize.define('personaUbicacion', {
    personaId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    ubicacionId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    numeroTel: {
      type: DataTypes.INTEGER,
    },
    numeroCel: {
      type: DataTypes.INTEGER,
    },
    codigoPais: {
      type: DataTypes.INTEGER
    },
  }, {});
  personaUbicacion.associate = function(models) {
    // associations can be defined here
  };
  return personaUbicacion;
};