
module.exports = (sequelize, DataTypes) => {
    const Producto = sequelize.define('Producto', {
        nombreProducto: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        descripcion: {
            type: DataTypes.STRING,
        },
        periodoGarantia: {
            type: DataTypes.INTEGER,
        },
        estado: {
            type: DataTypes.INTEGER,
        },
        precioMinimo: {
            type: DataTypes.FLOAT,
        },
        precioActual: {
            type: DataTypes.FLOAT,
        },
        categoriaId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
    }, {});
    Producto.associate = function (models) {
        Producto.belongsTo(models.Categoria, {
            foreignKey: 'categoriaId',
            as: 'categorias'
        });
    };
    return Producto;
};