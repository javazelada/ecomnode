
module.exports = (sequelize, DataTypes) => {
  const ProductoPedido = sequelize.define('ProductoPedido', {
    pedidoId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    productoId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    precioUnitario: {
      type:DataTypes.FLOAT,
      allowNull: false
    },
    cantidad: {
      type:DataTypes.INTEGER,
      allowNull: false
    },
  }, {});
  ProductoPedido.associate = function(models) {
    // associations can be defined here
  };
  return ProductoPedido;
};