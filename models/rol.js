module.exports = (sequelize, DataTypes) => {
  const Rol = sequelize.define('Rol', {
    nombreRol: {
      type: DataTypes.STRING,
      allowNull: false
    },
    descripcion: {
      type: DataTypes.STRING
    },
  }, {});
  Rol.associate = function (models) {
    Rol.hasMany(models.Usuario, {
      foreignKey: 'rolId'
    });
  };
  return Rol;
};