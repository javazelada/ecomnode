
module.exports = (sequelize, DataTypes) => {
  const Ubicacion = sequelize.define('Ubicacion', {
    direccionLinea1: {
      type: DataTypes.STRING,
      allowNull: false
    },
    direccionLinea2: {
      type: DataTypes.STRING,
    },
    ciudad: {
      type: DataTypes.STRING,
      allowNull: false
    },
    descripcion: {
      type: DataTypes.STRING,
    },
    paisId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
  }, {});
  Ubicacion.associate = function (models) {
    Ubicacion.belongsTo(models.Pais, {
      foreignKey: 'paisId',
      as: 'paises'
    });
    Ubicacion.hasMany(models.Almacen, {
      foreignKey: 'ubicacionId'
    });
  };
  return Ubicacion;
};