module.exports = (sequelize, DataTypes) => {
  const Usuario = sequelize.define('Usuario', {
    nombreUsuario: {
      type: DataTypes.STRING,
      allowNull: false
    },
    contrasena: {
      type: DataTypes.STRING,
      allowNull: false
    },
    rolId: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {});
  Usuario.associate = function (models) {
    Usuario.belongsTo(models.Rol, {
      foreignKey: 'rolId',
      as: 'rol'
    });
  };
  return Usuario;
};