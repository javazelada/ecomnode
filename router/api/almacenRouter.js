const router = require('express').Router();
const almacenController = require('../../controllers/AlmacenController');

router.get('/obtenerAlmacenes', almacenController.getAlmacen);
router.post('/guardarAlmacen', almacenController.saveAlmacen);
router.get('/:id', almacenController.getAlmacenById);

module.exports = router;