const router = require('express').Router();
const categoriaController = require('../../controllers/CategoriaController');

router.get('/obtenerCategoria', categoriaController.getCategorias);
router.post('/guardarCategoria', categoriaController.saveCategorias);
router.get('/:id', categoriaController.getCategoriaById);

module.exports = router;