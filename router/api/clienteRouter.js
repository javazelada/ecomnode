const router = require('express').Router();
const clienteController = require('../../controllers/ClienteController');

router.get('/obtenerCliente', clienteController.getCliente);
router.post('/guardarCliente', clienteController.saveCliente);
router.get('/:id', clienteController.getClienteById);

module.exports = router;