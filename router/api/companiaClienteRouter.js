const router = require('express').Router();
const companiaClienteController = require('../../controllers/CompaniaClienteController');

router.get('/obtenerCompania', companiaClienteController.getCompaniaCliente);
router.post('/guardarCompania', companiaClienteController.saveCompaniaCliente);
router.get('/:id', companiaClienteController.getCompaniaClienteById);

module.exports = router;