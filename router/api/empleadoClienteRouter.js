const router = require('express').Router();
const empleadoClienteController = require('../../controllers/EmpleadoClienteController');

router.get('/obtenerEmpleadoCliente', empleadoClienteController.getEmpleadoCliente);
router.post('/guardarEmpleadoCliente', empleadoClienteController.saveEmpleadoCliente);
router.get('/:id', empleadoClienteController.getEmpleadoClienteById);

module.exports = router;