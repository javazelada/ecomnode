const router = require('express').Router();
const empleadoController = require('../../controllers/EmpleadoController');

router.get('/obtenerEmpleados', empleadoController.getEmpleado);
router.post('/guardarEmpleado', empleadoController.saveEmpleado);
router.get('/:id', empleadoController.getEmpleadoById);

module.exports = router;