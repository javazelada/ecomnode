
const router = require('express').Router();

router.use('/categorias', require('./categoriaRouter'));
router.use('/productos', require('./productoRouter'));
router.use('/roles', require('./rolRouter'));
router.use('/usuarios', require('./usuarioRouter'));
router.use('/personas', require('./personaRouter'));
router.use('/empleados', require('./empleadoRouter'));
router.use('/informacion',require('./informacionRestringidaRouter'));
router.use('/pais', require('./paisRouter'));
router.use('/ubicaciones', require('./ubicacionRouter'));
router.use('/almacenes', require('./almacenRouter'));
router.use('/compania', require('./companiaClienteRouter'));
router.use('/empleadoCliente', require('./empleadoClienteRouter'));
router.use('/cliente', require('./clienteRouter'));
router.use('/pedido', require('./pedidoRouter'));

module.exports = router;
