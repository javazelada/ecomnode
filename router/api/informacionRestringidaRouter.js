const router = require('express').Router();
const informacionRestringidaController = require('../../controllers/InformacionRestringidaController');

router.get('/obtenerInformacionPersona', informacionRestringidaController.getInformacionRestringida);
router.post('/guardarInformacionPersona', informacionRestringidaController.saveInformacionRestringida);
router.get('/:id', informacionRestringidaController.getInformacionRestringidaById);

module.exports = router;