const router = require('express').Router();
const paisController = require('../../controllers/PaisController');

router.get('/obtenerPaises', paisController.getPais);
router.post('/guardarPais', paisController.savePais);
router.get('/:id', paisController.getPaisById);

module.exports = router;