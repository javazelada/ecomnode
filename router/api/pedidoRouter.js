const router = require('express').Router();
const pedidoController = require('../../controllers/PedidoController');

router.get('/obtenerPedidos', pedidoController.getPedido);
router.post('/guardarPedido', pedidoController.savePedido);
router.get('/:id', pedidoController.getPedidoById);

module.exports = router;    