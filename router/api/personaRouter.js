const router = require('express').Router();
const personaController = require('../../controllers/PersonaController');

router.get('/obtenerPersonas', personaController.getPersona);
router.post('/guardarPersona', personaController.savePersona);
router.get('/:id', personaController.getPersonaById);

module.exports = router;