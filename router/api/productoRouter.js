const router = require('express').Router();
const productoController = require('../../controllers/ProductoController');

router.get('/obtenerProductos', productoController.getProductos);
router.post('/guardarProductos', productoController.saveProductos);
router.get('/:id', productoController.getProductoById);
module.exports = router;