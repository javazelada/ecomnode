const router = require('express').Router();
const rolController = require('../../controllers/RolController');

router.get('/obtenerRoles', rolController.getRol);
router.post('/guardarRol', rolController.saveRol);
router.get('/:id', rolController.getRolById);

module.exports = router;