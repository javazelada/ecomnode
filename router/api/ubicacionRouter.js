const router = require('express').Router();
const ubicacionController = require('../../controllers/UbicacionController');

router.get('/obtenerUbicaciones', ubicacionController.getUbicacion);
router.post('/guardarUbicacion', ubicacionController.saveUbicacion);
router.get('/:id', ubicacionController.getUbicacionById);

module.exports = router;