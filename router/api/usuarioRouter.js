const router = require('express').Router();
const usuarioController = require('../../controllers/UsuarioController');

router.get('/obtenerUsuarios', usuarioController.getUsuario);
router.post('/guardarUsuario', usuarioController.saveUsuarios);
router.get('/:id', usuarioController.getUsuarioById);

module.exports = router;