
module.exports = {
	up: (queryInterface, Sequelize) => {

		return queryInterface.bulkInsert('Categoria',
			[
				{
					nombreCategoria: 'dia de la madre',
					descripcion: 'flores para alegrar a la madre',
					createdAt: new Date(),
					updatedAt: new Date(),

				},
				{
					nombreCategoria: 'san valentin',
					descripcion: 'flores para enamorados',
					createdAt: new Date(),
					updatedAt: new Date(),
				},
				{
					nombreCategoria: 'para obsequiar',
					descripcion: 'flores para todas las ocaciones',
					createdAt: new Date(),
					updatedAt: new Date(),
				},
			], {});
	},

	down: (queryInterface, Sequelize) => {

		return queryInterface.bulkDelete('Categoria', null, {});
	}
};
