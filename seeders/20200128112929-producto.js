
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Productos',
      [
        {
          categoriaId: 1,
          nombreProducto: 'flores paradisiacas',
          descripcion: 'flores para alegrar el hogar',
          periodoGarantia: 1,
          estado: 1,
          precioMinimo: 15,
          precioActual: 23,
          createdAt: new Date(),
					updatedAt: new Date(),
        },
        {
          categoriaId: 1,
          nombreProducto: 'flores con florero de vidrio',
            descripcion: 'flores con florero de vidrio para dar mas elegancia',
          periodoGarantia: 1,
          estado: 1,
          precioMinimo: 67,
          precioActual: 80,
          createdAt: new Date(),
					updatedAt: new Date(),
        },
        {
          categoriaId: 2,
          nombreProducto: 'flores en forma de corazones',
          descripcion: 'flores para enamorados',
          periodoGarantia: 1,
          estado: 1,
          precioMinimo: 21,
          precioActual: 28,
          createdAt: new Date(),
					updatedAt: new Date(),
        },
        {
          categoriaId: 2,
          nombreProducto: 'flores con peluche',
          descripcion: 'flores para una ocacion especial como enamorados',
          periodoGarantia: 1,
          estado: 1,
          precioMinimo: 80,
          precioActual: 90,
          createdAt: new Date(),
					updatedAt: new Date(),
        },
      ], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Productos', null, {});
  }
};

