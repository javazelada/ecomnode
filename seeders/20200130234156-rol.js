
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Rols',
      [
        {
          // 1
          nombreRol: 'usuario',
          descripcion: 'puede ver las publicaciones',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          // 2
          nombreRol: 'admin',
          descripcion: 'puede ver y editar todas las publicaciones',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          // 3
          nombreRol: 'vendedor',
          descripcion: 'puede anadir y modificar las publicaciones',
          createdAt: new Date(),
          updatedAt: new Date(),
        },

      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Rols', null, {});
  }
};
