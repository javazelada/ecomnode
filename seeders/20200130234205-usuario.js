
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Usuarios',
      [
        {
          nombreUsuario: 'javazelada@gmail.com',
          contrasena: '12345',
          rolId: 1, // usuario
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nombreUsuario: 'miocarlita@gmail.com',
          contrasena: '12345',
          rolId: 2, // admin
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nombreUsuario: 'carlos@gmail.com',
          contrasena: '12345',
          rolId: 3, // vendedor
          createdAt: new Date(),
          updatedAt: new Date(),
        },

      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Usuarios', null, {});
  }
};
