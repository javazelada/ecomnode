
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Personas',
      [
        {
          nombres: 'javier andres',
          primerApellido: 'zelada',
          segundoApellido: 'yujra',
          nickname: 'javaz',
          usuarioId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nombres: 'carla uyuki',
          primerApellido: 'cuba',
          segundoApellido: 'botitano',
          nickname: 'mio',
          usuarioId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nombres: 'carlos',
          primerApellido: 'valverde',
          segundoApellido: 'fonse',
          nickname: 'carlos',
          usuarioId: 3,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Personas', null, {});
  }
};
