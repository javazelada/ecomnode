
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Empleados',
      [
        {
          fechaInicio: '2000-12-01',
          salario: 4000,
          cargo: 1,
          personaId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          fechaInicio: '2011-12-01',
          salario: 5000,
          cargo: 2,
          personaId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          fechaInicio: '2018-12-01',
          salario: 6000,
          cargo: 3,
          personaId: 3,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('People', null, {});
  }
};
