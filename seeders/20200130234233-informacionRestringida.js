
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('InformacionRestringidas',
      [
        {
          fechaNacimiento: '1988-04-21',
          cedulaIdentidad: '6800356lp',
          fechaContrato: '2000-12-01',
          personaId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          fechaNacimiento: '1986-12-26',
          cedulaIdentidad: '676373lp',
          fechaContrato: '2000-12-01',
          personaId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          fechaNacimiento: '1999-12-21',
          cedulaIdentidad: '684444lp',
          fechaContrato: '2018-12-01',
          personaId: 3,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('InformacionRestringidas', null, {});
  }
};
