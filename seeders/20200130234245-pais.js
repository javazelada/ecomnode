
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Pais',
      [
        {
          nombrePais: 'Bolivia',
          codigoLenguaje: 'ES',
          idiomaPreferido: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nombrePais: 'Peru',
          codigoLenguaje: 'ES',
          idiomaPreferido: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Pais', null, {});
  }
};
