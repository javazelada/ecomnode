
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Ubicacions',
      [
        {
          direccionLinea1: 'alto san antonio calle union # 95',
          ciudad: 'la paz',
          descripcion: 'casa de ladrillos',
          paisId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          direccionLinea1: 'direccion 1',
          ciudad: 'la paz',
          descripcion: 'casa almacen',
          paisId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          direccionLinea1: 'direccion 2',
          ciudad: 'la paz',
          descripcion: 'casa almacen',
          paisId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          direccionLinea1: 'direccion 3',
          ciudad: 'la paz',
          descripcion: 'casa almacen',
          paisId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          direccionLinea1: 'direccion 4',
          ciudad: 'la paz',
          descripcion: 'casa almacen',
          paisId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Ubicacions', null, {});

  }
};
