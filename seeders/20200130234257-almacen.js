
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Almacens',
      [
        {
          nombreAlmacen: 'central',
          ubicacionId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nombreAlmacen: 'almacen 1',
          ubicacionId: 3,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nombreAlmacen: 'almacen 2',
          ubicacionId: 4,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Almacens', null, {});
  }
};
