
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('companiaClientes',
      [
        {
          nombreCompania: 'umsa',
          ubicacionId: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          nombreCompania: 'empresa1',
          ubicacionId: 2,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          nombreCompania: 'empresa2',
          ubicacionId: 3,
          createdAt: new Date(),
          updatedAt: new Date()
        },
      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('companiaClientes', null, {});
  }
};
