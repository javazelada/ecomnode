
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('EmpleadoClientes',
      [
        {
          cargo: 'empleado',
          departamento: 'depa1',
          companiaClienteId: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          cargo: 'empleado2',
          departamento: 'depa2',
          companiaClienteId: 2,
          createdAt: new Date(),
          updatedAt: new Date()
        },

      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('EmpleadoClientes', null, {});
  }
};
