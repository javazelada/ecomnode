
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Clientes',
      [
        {
          personaId: 1,
          empleadoClienteId: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          personaId: 2,
          empleadoClienteId: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Clientes', null, {});
  }
};
