
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Pedidos',
      [
        {
          fechaPedido: '2019-12-22',
          codigoPedido: 12345,
          estadoPedido: 2,
          totalPedido: 1,
          monedaPedido: 2,
          codigoPromocion: 'DEI342D45',
          clienteId: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          fechaPedido: '2019-10-22',
          codigoPedido: 12345,
          estadoPedido: 2,
          totalPedido: 1,
          monedaPedido: 2,
          codigoPromocion: 'DEI342D45',
          clienteId: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          fechaPedido: '2018-05-22',
          codigoPedido: 12345,
          estadoPedido: 2,
          totalPedido: 1,
          monedaPedido: 2,
          codigoPromocion: 'DEI342D45',
          clienteId: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },

      ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Pedidos', null, {});
  }
};
